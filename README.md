# workman-swoole

## 介绍
同时支持wokerman、webman、swoole、laravel、thinkphp 的 php82 镜像，其中还安装了能够提升workman性能的event扩展，PHP扩展通过pecl 和 docker-php-ext-enable 进行安装，容器内置了nginx和supervisor支持laravel、thinkphp运行。

## 已安装扩展

```
bcmath
Core
ctype
curl
date
dom
event
fileinfo
filter
gd
hash
iconv
imagick
json
libxml
mbstring
mcrypt
mysqlnd
openssl
pcntl
pcre
PDO
pdo_mysql
pdo_pgsql
pdo_sqlite
Phar
posix
random
readline
redis
Reflection
session
SimpleXML
sockets
sodium
SPL
sqlite3
standard
swoole
tokenizer
xlswriter
xml
xmlreader
xmlwriter
Zend OPcache
zlib

[Zend Modules]
Zend OPcache
```