FROM php:8.2-fpm-alpine

LABEL Maintainer="david <367013672@qq.com>"
LABEL Description="supervisor container with PHP ^8.2 based on Alpine Linux."
LABEL Version="8.2"

ENV TZ "Asia/Shanghai"
# 时区
RUN echo ${TZ} >/etc/timezone

# 使用国内镜像（mirrors.aliyun.com ｜ mirrors.ustc.edu.cn）
ARG package_url=mirrors.ustc.edu.cn
RUN if [ $package_url ] ; then sed -i "s/dl-cdn.alpinelinux.org/${package_url}/g" /etc/apk/repositories ; fi

RUN apk update && apk add --no-cache $PHPIZE_DEPS \
        curl-dev \
        imagemagick-dev \
        libtool \
        libxml2-dev \
        postgresql-dev \
        sqlite-dev \
	    libmcrypt \
	    libmcrypt-dev \
        freetype-dev \
        libjpeg-turbo-dev \
        libpng-dev \
        libevent \
        libevent-dev \
        zlib-dev libzip-dev

RUN apk add --no-cache \
        linux-headers \
        curl \
        zip \
        git \
        wget \
        imagemagick \
        mysql-client \
        postgresql-libs \
        bash

RUN docker-php-ext-install -j$(nproc) sockets bcmath pdo_mysql pcntl pdo_pgsql pdo_sqlite zip

RUN docker-php-ext-configure opcache --enable-opcache && docker-php-ext-install opcache

RUN pecl install imagick\
    && docker-php-ext-enable imagick \
    && pecl install mcrypt-1.0.7 \
    && docker-php-ext-enable mcrypt \
    && pecl install redis-5.3.4 \
    && docker-php-ext-enable redis \
    && pecl install event-3.1.3 \
    && docker-php-ext-enable --ini-name zz-event.ini event \
    && pecl install swoole-5.1.1 \
    && docker-php-ext-enable swoole && echo 'swoole.use_shortname=Off'>> /usr/local/etc/php/conf.d/docker-php-ext-swoole.ini \
    && pecl install xlswriter-1.5.5 \
    && docker-php-ext-enable xlswriter


RUN apk update \
    && docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install -j$(nproc) gd

COPY uploads.ini $PHP_INI_DIR/conf.d

RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"


RUN php -r "copy('https://install.phpcomposer.com/installer', 'composer-setup.php');"
RUN php composer-setup.php
RUN mv composer.phar /usr/local/bin/composer
RUN composer config -g repo.packagist composer https://mirrors.aliyun.com/composer/

ENV NGX_WWW_ROOT /var/www
RUN apk add nginx
RUN mkdir -p /run/nginx && touch /run/nginx/nginx.pid

RUN apk add supervisor
COPY supervisor.d /etc/supervisor.d/

EXPOSE 80 443 9000

RUN rm -rf /var/cache/apk/* /tmp/*

COPY nginx.conf /etc/nginx
COPY conf.d /etc/nginx/conf.d/
COPY www "${NGX_WWW_ROOT}"

COPY entrypoint.sh /var/www/

RUN chmod +x /var/www/entrypoint.sh

ENTRYPOINT ["/var/www/entrypoint.sh"]

WORKDIR /var/www